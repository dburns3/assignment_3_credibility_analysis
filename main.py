#!/usr/bin/env python3

from src.credibilityAnalyzer import CredibilityAnalyzer

def main():
	# Read in sensing matrix file
	with open('data/SCMatrix_Submit') as f:
		sensing_matrix_input = f.read()
	
	# Initialize CredibilityAnalyzer
	credibility_analyzer = CredibilityAnalyzer(sensing_matrix_input)

if __name__ == '__main__':
	main()
