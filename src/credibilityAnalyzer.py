from random import uniform

class CredibilityAnalyzer():
	
	def __init__(self, sensing_matrix_input):
		self.number_of_variables = 0
		self.matrix_dict = {}
		self.matrix = self.initializeMatrix(sensing_matrix_input)
		self.s_i = self.initialS_i()
		self.truthCalculation(self.matrix_dict)
	
	def truthCalculation(self, matrix_dict):
		solution = {}
		i = 0
		number_of_sources = len(list(self.matrix_dict.keys()))
		total_number_of_observations = len(self.matrix[10])
		a = self.s_i
		b = self.calculateB_i(self.number_of_variables)
		d = uniform(0, 1)

		while i < 20:
			i += 1
			j = 1
			while j <= self.number_of_variables:
				i2 = 1
				A_t_j = 1
				B_t_j = 1
				while i2 <= number_of_sources:
					A_t_j *= pow(a[i2], self.matrix[i2][j]) * pow((1-a[i2]), (1-self.matrix[i2][j]))
					B_t_j *= pow(b[i2], self.matrix[i2][j]) * pow((1-b[i2]), (1-self.matrix[i2][j]))
					i2 += 1
				solution[j] = float(A_t_j*d) / float(A_t_j*d+B_t_j*(1-d))
				j += 1
			total_of_solution = sum(solution.values())
			solution_helper = {}
			solution_helper_counter = 1
			while solution_helper_counter <= number_of_sources:
				solution_helper[solution_helper_counter] = len(self.matrix_dict[solution_helper_counter])
				observation_of_solution = self.computeSolution(solution, solution_helper_counter)

				if float(total_of_solution) == 0: a[i2] = 0
				else: a[i2] = observation_of_solution / float(total_of_solution)
				if float(self.number_of_variables - total_of_solution) == 0: b[i2] = 0
				else: b[i2] = (solution_helper[solution_helper_counter]-observation_of_solution) / \
					float(self.number_of_variables - total_of_solution)
				if float(self.number_of_variables) == 0: d = 0
				else: d = float(total_of_solution)/float(self.number_of_variables)
				solution_helper_counter += 1

		final_output = {}
		for i, j in solution.items():
			if j >= 0.5:
				final_output[i] = 1
			else:
				final_output[i] = 0
		for i, j in final_output.items():
			print('{0}: {1}'.format(i, j))

	def computeSolution(self, solution, solution_helper_counter):
		temp = 0
		for counter in self.matrix_dict[solution_helper_counter]:
			tempObservation = solution[counter]
			temp += tempObservation
		return tempObservation
	
	def calculateB_i(self, total_number_of_observations):
		b_i = {}
		for counter in range(len(self.matrix_dict.keys())):
			num_observations = len(list(self.matrix_dict.values())[counter])
			probability = (num_observations / self.number_of_variables) / 2
			b_i[counter+1] = probability
		return b_i
	
	def initialS_i(self):
		s_i = {}
		for counter in range(len(self.matrix_dict.keys())):
			num_observations = len(list(self.matrix_dict.values())[counter])
			probability = num_observations/self.number_of_variables
			s_i[counter+1] = probability
		return s_i
	
	def initializeMatrix(self, sensing_matrix_input):
		sensing_matrix_input = sensing_matrix_input.split('\n')[:-1]
		matrix = []

		# Create matrix
		max_row = 0
		max_column = 0
		for line in sensing_matrix_input:
			source, variable_id = [int(data) for data in line.split(',')]
			if source > max_row: max_row = source
			if variable_id > max_column: max_column = variable_id
		matrix = [[0 for _ in range(max_column+1)] for _ in range(max_row+1)]
		self.number_of_variables = max_column

		# Set 1's in matrix
		for line in sensing_matrix_input:
			source, variable_id = [int(data) for data in line.split(',')]
			matrix[source][variable_id] = 1
		
		# Create adjacency list
		for line in sensing_matrix_input:
			source, variable_id = [int(data) for data in line.split(',')]
			if source not in self.matrix_dict: self.matrix_dict[source] = [variable_id]
			else: self.matrix_dict[source].append(variable_id)

		# Return matrix
		return matrix
